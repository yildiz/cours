package fr.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Controler {

    @Autowired
    private RestClientService service;

    @ResponseBody
    @RequestMapping(value = "/dossiers/{id}/files/{fileId}", method = RequestMethod.GET)
    public String getDossier(@PathVariable String id, @PathVariable String fileId) {
        return "Dossier:" + id + " and file:" + fileId;
    }

    @ResponseBody
    @RequestMapping(value = "/dossiers/{id}/files/{fileId}", method = RequestMethod.DELETE)
    public void deleteDossier(@PathVariable String id, @PathVariable String fileId) {

    }


    @RequestMapping("/dossiers/{id}/page")
    public String displayDossier(Model model, @PathVariable String id) {
        model.addAttribute("id", id);
        return "dossier";
    }

    @ResponseBody
    @RequestMapping("/rooms")
    public String getRooms() {
        return this.service.getRoomList().toString();
    }

}
